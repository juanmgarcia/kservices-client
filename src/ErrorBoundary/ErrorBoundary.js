import React, {Component} from 'react'
class ErrorBoundry extends Component {
    state = {
        hasError: false,
        errorMessage: ''
    }
    componentDidCatch = (error, info) => {
        this.setState({hasError: true, errorMessage: error})
    }
    render () {
        return (
            <span>
                {this.state.hasError && (<span>{this.state.errorMessage}</span>)}
                {!this.state.hasError && (<span>{this.props.children}</span>)}
            </span>
        )
    }
}

export default ErrorBoundry