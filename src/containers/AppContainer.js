
import withRoot from '../modules/withRoot';
// Redux
import {connect} from 'react-redux'
// --- Post bootstrap -----
import React, {Component} from 'react';
import ProductCategories from '../modules/views/ProductCategories';
import ProductSmokingHero from '../modules/views/ProductSmokingHero';
import AppFooter from '../modules/views/AppFooter';
import ProductHero from '../modules/views/ProductHero';
import ProductValues from '../modules/views/ProductValues';
import ProductHowItWorks from '../modules/views/ProductHowItWorks';
import ProductCTA from '../modules/views/ProductCTA';
import AppAppBar from '../modules/views/AppAppBar';
import AppFormDialog from '../modules/views/AppFormDialog';

class Index extends Component {
  state = {
    loginModal: false
  }
  handleLogin = (data) => {
    this.props.login(data).then(result => {
      console.log('este es el result', result)
    }).catch(err => {
      console.log('error', err)
    });
  }
  closeOpenLogin = (value) => {
    this.setState(state => ({
      ...state,
      loginModal: value
    }))
  }
  render () {
    return (
      <React.Fragment>
        <AppAppBar handleOpen={() => this.closeOpenLogin(true)}/>
        <ProductHero />
        <ProductValues />
        <ProductCategories />
        <ProductHowItWorks />
        <ProductCTA />
        <ProductSmokingHero />
        <AppFormDialog open={this.state.loginModal} handleLogin={this.handleLogin} handleClose={() => this.closeOpenLogin(false)} />
        <AppFooter />
      </React.Fragment>
    );
  }
  componentDidMount () {
  }
}
const mapStateToProps = state => ({
});

const mapDispatchToProps = (dispatch, {services}) => ({
  login: (data) => {
    return dispatch(services.authentication.create({
      ...data,
      strategy: 'local'
    }));
  }
});

export default connect(mapStateToProps, mapDispatchToProps)(withRoot(Index));