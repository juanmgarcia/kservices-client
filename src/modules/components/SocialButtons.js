import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import PropTypes from 'prop-types'
import Grid from '@material-ui/core/Grid';
import Button from '@material-ui/core/Button';
import classNames from 'classnames'
const styles = theme => ({
  root: {
  },
  btnWrapper: {
    textAlign: 'center'
  },
  button: {
    margin: '5px',
    width: '100%'
  },
  facebook: {
    backgroundColor: '#3b5998',
    color: '#FFF'
  },
  google: {
    backgroundColor: '#008744',
    color: '#FFF'
  }
});
const SocialButtons = (props) => {
    const {classes} = props;
    return (
     <Grid 
       container 
       spacing={8} 
       direction="row"
       justify="center"
       alignItems="center"
       >
        <Button onClick={() => props.onBtnClicked('facebook')} size="medium" className={classNames(classes.button, classes.facebook)}>
            Facebook
        </Button>
        <Button onClick={() => props.onBtnClicked('google')} size="medium"  className={classNames(classes.button, classes.google)}>
            Google
        </Button>
    </Grid>
    );
}
SocialButtons.propTypes = {
  onBtnClicked: PropTypes.func.isRequired
};

export default withStyles(styles)(SocialButtons);