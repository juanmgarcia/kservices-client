import React from 'react';
import PropTypes from 'prop-types';
import Dialog from '@material-ui/core/Dialog';
import DialogContent from '@material-ui/core/DialogContent';
import Toolbar, { styles as toolbarStyles } from './Toolbar';
import Divider from '@material-ui/core/Divider';
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  title: {
    fontSize: 24,
  },
  placeholder: toolbarStyles(theme).root,
  toolbar: {
    justifyContent: 'space-between',
  },
  left: {
    flex: 1,
  },
  leftLinkActive: {
    color: theme.palette.common.white,
  },
  right: {
    flex: 1,
    display: 'flex',
    justifyContent: 'flex-end',
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    marginLeft: theme.spacing.unit * 3,
  },
  linkSecondary: {
    color: theme.palette.secondary.main,
  },
  icon: {
    cursor: 'pointer'
  }
})
const FormDialog = (props) => {
  const {classes} = props;
  return (
  <Dialog
    open={props.open}
    onClose={props.handleClose}
    aria-labelledby="form-dialog-title"
  >
    <Toolbar className={classes.toolbar}>
      <div className={classes.left} />
            <span className={classes.title}>{props.title}</span>
        <div className={classes.right}>
        <Icon onClick={props.handleClose} className={classes.icon}>
            close
        </Icon>
        </div>
    </Toolbar>
    <Divider />

    <DialogContent>
       {props.children}
    </DialogContent>
  </Dialog>
  );
}

FormDialog.propTypes = {
  open: PropTypes.bool.isRequired,
  handleClose: PropTypes.func.isRequired,
  title: PropTypes.string.isRequired,
  classes: PropTypes.object.isRequired,
}

export default withStyles(styles)(FormDialog);