import React from 'react';
import FormDialog from '../components/FormDialog';
import LoginUserForm from './../components/forms/users/LoginUserForm';

const AppFormDialog = (props) => {
    return (
      <FormDialog
        open={props.open}
        handleClose={props.handleClose}
        title="Log in"
        maxWidth='lg'
        fullWidth={false}
      >
      <LoginUserForm handleLogin={props.handleLogin}/>
      </FormDialog>
);
}

export default AppFormDialog;