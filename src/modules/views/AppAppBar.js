import React, { Component } from 'react';
import PropTypes from 'prop-types';
import classNames from 'classnames';
import { withStyles } from '@material-ui/core/styles';
import Link from '@material-ui/core/Link';
import AppBar from '../components/AppBar';
import Toolbar from './../components/Toolbar';

const styles = theme => ({
  title: {
    fontSize: 24,
  },
  toolbar: {
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
  },
  toolbarContainer: {
    display: 'flex',
    width: '70%',
    justifyContent: 'space-between'
  },
  left: {
  },
  leftLinkActive: {
    color: theme.palette.common.white,
  },
  right: {
    display: 'flex'
  },
  rightLink: {
    fontSize: 16,
    color: theme.palette.common.white,
    marginLeft: theme.spacing.unit * 3,
  },
  linkSecondary: {
    color: theme.palette.secondary.main,
  },
  darkToolbar: {
    backgroundColor: theme.palette.primary
  },
  transparentToolbar: {
    backgroundColor: 'transparent'
  }
});

class AppAppBar extends Component {
  state = {
    lastScrollY: 0,
    slide: 0,
    currentBackground: 'transparent'
  }
  componentWillMount () {
    window.addEventListener('scroll', this.handleScroll)
  }
  componentWillUnmount () {
    window.removeEventListener('scroll', this.handleScroll, { useCapture: true })
  }
  handleScroll = () => {
    const { lastScrollY } = this.state;
    const currentScrollY = window.scrollY;
    if (currentScrollY > lastScrollY) {
      this.setState(
        {
          slide: '-70px',
        }
      );
    } else {
      this.setState(
        {
          slide: '0px',
        }
      );
    }
    if (currentScrollY === 0) {
      this.setState({
        currentBackground: 'transparent'
      })
    } else if (currentScrollY > 70) {
      this.setState({
        currentBackground: 'black'
      })
    }
    this.setState({ lastScrollY: currentScrollY });
  }
  render () {
    return (
      <div>
        <AppBar
          style={{
            transform: `translate(0, ${this.state.slide})`,
            transition: 'transform 90ms linear',
            backgroundColor: this.state.currentBackground
          }}
          position="fixed">
          <Toolbar className={this.props.classes.toolbar}>
            <div className={this.props.classes.toolbarContainer}>
              <Link
                variant="h6"
                underline="none"
                color="inherit"
                className={this.props.classes.title}
                href="/"
              >
                {'KSERVICES'}
              </Link>
              <div className={this.props.classes.right}>
                <Link
                  variant="h6"
                  underline="none"
                  className={classNames(this.props.classes.rightLink)}
                  href="/signup"
                >
                  {'Sign Up'}
                </Link>
                <Link
                  color="inherit"
                  variant="h6"
                  underline="none"
                  className={this.props.classes.rightLink}
                  onClick={this.props.handleOpen}
                  href="#"
                >
                  {'Log In'}
                </Link>
              </div>
            </div>
          </Toolbar>
        </AppBar>
        <div className={this.props.classes.placeholder} />
      </div>
    );
  }
}

AppAppBar.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(AppAppBar);
