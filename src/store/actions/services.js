export const STORE_SERVICES = 'STORE_SERVICES'

// Actions creators
export const setServices = (result) => {
    return {
        type: STORE_SERVICES,
        value: result
    };
}
export const getAsyncServices = (res) => {
    return dispatch => {
        dispatch (setServices);
    };
}