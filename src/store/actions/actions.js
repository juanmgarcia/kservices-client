export const CHANGE_ENV = 'CHANGE_ENV'

// Actions creators
export const changeEnv = () => {
    return {type: CHANGE_ENV}
}