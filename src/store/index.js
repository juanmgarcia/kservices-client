
import { createStore, applyMiddleware } from 'redux';
import rootReducer from './reducers';
import middlewares from './middlewares';

export default function configureStore(reduxifiedServices) {
  const createStoreWithDevTools = window.devToolsExtension
    ? window.devToolsExtension()(createStore)
    : createStore;

  const createStoreWithMiddlewares = applyMiddleware(...middlewares)(createStoreWithDevTools);

  return createStoreWithMiddlewares(rootReducer(reduxifiedServices));
}