import * as actionTypes from '../actions/actions';
const enviroment = require('../../config/enviroment.json');
const initialState = {
  currentEnv: enviroment[enviroment.currentEnviroment].backend.urlBase
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_ENV :
            return {
                ...state,
                currentEnv: ''
            }
        default: return state
    }
}
export default reducer;