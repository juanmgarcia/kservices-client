import * as actionTypes from '../actions/services'

const initialState = {
    services: ['hi', 'hello']
};

const reducer = (state = initialState, action) => {
    switch (action.type) {
        case actionTypes.STORE_SERVICES :
            return {
                ...state,
                services: action.value
            }
        default: return state
    }
}
export default reducer;