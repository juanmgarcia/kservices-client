
import { combineReducers } from 'redux';
import env from './../reducers/initial'

export default function (reduxifiedServices) {
  return combineReducers({
    users: reduxifiedServices.users.reducer,
    services: reduxifiedServices.services.reducer,
    customers: reduxifiedServices.customers.reducer,
    profiles: reduxifiedServices.profiles.reducer,
    reviews: reduxifiedServices.reviews.reducer,
    env
  });
}
