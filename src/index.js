//React imports
import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter } from 'react-router-dom';
import configureStore from './store'
// Feathers Client
import feathersClient from './feathers-client';
import { Provider } from 'react-redux';
// Feathers Redux
import reduxifyServices, {getServicesStatus} from 'feathers-redux';
// Others
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
//Creating redux reducers from feathers services
const services = reduxifyServices(feathersClient, ['users','services', 'customers', 'profiles', 'reviews', 'authentication']);
const store = configureStore(services);
// React render whole app
ReactDOM.render(<BrowserRouter><Provider store={store}><App services={services} getServicesStatus={getServicesStatus}/></Provider></BrowserRouter>, document.getElementById('root'));
serviceWorker.unregister();
