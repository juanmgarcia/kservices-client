import React, { Component } from 'react';
import { Route } from 'react-router-dom'
import Home from './containers/AppContainer'
import LoginContainer from './containers/LoginContainer'
import UserRegistrationContainer from './containers/UserRegistrationContainer'
import WithStyle from './modules/components/hoc/WithStyle'
import './App.css';

class App extends Component {
  render() {
    return (
      <WithStyle classes="App">
        <Route path="/" exact render={() => (<Home services={this.props.services}></Home>)}/>
        <Route path="/login" render={() => (<LoginContainer services={this.props.services}></LoginContainer>)}/>
        <Route path="/signup" render={() => (<UserRegistrationContainer services={this.props.services}></UserRegistrationContainer>)}/>
      </WithStyle>
    );
  }
}

export default App;
